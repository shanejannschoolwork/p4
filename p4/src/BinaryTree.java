///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  GuessingGame.java
// File:             BinaryTree.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann   jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      002
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 *  A data structure that acts as a Binary Tree. Each node should have at
 *  most two children. Exceptions are thrown when illegal operations are
 *  done on a binary node.
 *
 * <p>Bugs: None known
 *
 * @author Shane Jann
 */
public class BinaryTree<E> {
	BinaryTreenode<E> root;      //The first node in the binary tree
	BinaryTreenode<E> currNode;  //The node that is currently referenced
	
	
	
	/**
	 * Creates an empty BinaryTree where the root is null. Initialize by 
	 * calling changeCurrent(E data)
	 */
	public BinaryTree(){
		root = null;
	}
	/**
	 * Creates a BinaryTree with root data and sets the currNode reference
	 * to the root.
	 * 
	 * @param data the key stored in the root node.
	 */
	public BinaryTree(E data){
		root = new BinaryTreenode<E>(data);
		root.setData(data);
		currNode = root; 
	}
	
	
	/**
	 * Resets the currNode reference to the root.
	 */
	void start(){
		currNode = root;
	}
	
	/**
	 * Returns the data in the current node
	 * 
	 * @return data key in currNode
	 * @throws IllegalBinaryTreeOpException if the current node is null
	 */
	E getCurrent() throws IllegalBinaryTreeOpException{
		if(currNode == null) throw new IllegalBinaryTreeOpException("Cannot " +
				"get current node's data; there is no current node.");
		
		return currNode.getData();
	}
	
	/**
	 * Changes currNode reference to left child
	 * 
	 * @throws IllegalBinaryTreeOpException if currNode has no left child
	 */
	void goLeft() throws IllegalBinaryTreeOpException{
		if(currNode.getLeft() == null) throw new IllegalBinaryTreeOpException(
				"Cannot go left, the current node has no left child");
		currNode = currNode.getLeft();
	}
	
	/**
	 * Changes currNode reference to right child
	 * 
	 * @throws IllegalBinaryTreeOpException if currNode has no right child
	 */
	void goRight() throws IllegalBinaryTreeOpException{
		if(currNode.getRight() == null) throw new IllegalBinaryTreeOpException(
				"Cannot go right, the current node has no right child");
		currNode = currNode.getRight();
	}
	
	/**
	 * Returns true if the currNode reference has no children, false otherwise
	 * 
	 * @return true if currNode has no children, false otherwise
	 */
	boolean isLeaf(){
		return currNode.getLeft() == null && currNode.getRight() == null;
	}
	
	/**
	 * Changes the data in the currNode reference to 'data'
	 * 
	 * @param data the key that currNode will be changed to
	 */
	void changeCurrent(E data){
		if(root == null){
			root = new BinaryTreenode<E>(data);
			currNode = root;
		}
		currNode.setData(data);
	}
	
	/**
	 * adds a BinaryTreenode as the right child of currNode
	 * 
	 * @param data the data of the new right child
	 * @throws IllegalBinaryTreeOpException if there already exists a child
	 */
	void addRightChild(E data) throws IllegalBinaryTreeOpException{
		if(currNode.getRight() != null){
			throw new IllegalBinaryTreeOpException("Cannot add right child;" +
					" there already exists a right child.");
		}
		currNode.setRight(data);
	}
	
	/**
	 * adds a BinaryTreenode as the left child of currNode
	 * 
	 * @param data the data of the new left child
	 * @throws IllegalBinaryTreeOpException if there already exists a child
	 */
	void addLeftChild(E data) throws IllegalBinaryTreeOpException{
		if(currNode.getLeft() != null){
			throw new IllegalBinaryTreeOpException("Cannot add left child;" +
					" there already exist a left child");
		}
		currNode.setLeft(data);
	}
	
	/**
	 * With the help of the Auxiliary method print(BinaryTreenode<E>,
	 * int) this prints a BinaryTree in pre-order, starting at the root.
	 */
	void print(){
		print(root, 0);
	}
	
	/**
	 * The helper method for print(), this takes in an input of a binary
	 * tree and how many spaces it should be shifted over. Implemented
	 * with direct recursion
	 * @param b the binary tree to be printed
	 * @param spaces how many spaces the current level should be
	 * printed with
	 */
	private void print(BinaryTreenode<E> b, int spaces){
		if(b == null) return;
		for(int i = 0; i < spaces; i++){
			System.out.print(" ");
		}
		System.out.println(b.getData());
		print(b.getLeft(), spaces + 3);
		print(b.getRight(), spaces + 3);
	}
	
	
}
