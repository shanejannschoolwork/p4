///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  GuessingGame.java
// File:             IllegalBinaryTreeOpException.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann   jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      002
//////////////////////////// 80 columns wide //////////////////////////////////


@SuppressWarnings("serial")
/**
 * Checked Exception class thrown whenever an Illegal Binary Operation
 * occurs in a program
 *
 * <p>Bugs: None known
 *
 * @author Shane Jann
 */
public class IllegalBinaryTreeOpException extends Exception {
	
	/**
	 * Constructor called when an exception is thrown. The user should include
	 * a message, so the console can display a specific error.
	 * @param message the message that should be displayed.
	 */
	public IllegalBinaryTreeOpException(String message){
		System.out.println(message);
	}
}
