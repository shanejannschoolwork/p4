///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:            GuessingGame.java
// Files:            IllegalBinaryTreeOpException.java, BinaryTree.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann  
// Email:            jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      002
//////////////////////////// 80 columns wide /////////////////////////////////

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Simulates the famous game "20 Questions". Can take in both an input file or
 * regular user input to form the 20 Questions game. The program questions
 * the user until a guess can be made.
 *
 * <p>Bugs: (Possible Bug)The program works well on my computer and through
 * a remote desktop connection, but through my WinSCP program, in the 
 * terminal, the program would not respond when given no additional arguments.
 * Shouldn't be an issue though.
 *
 * @author Shane Jann
 */
public class GuessingGame {

	private static BinaryTree<String> bTree;			   //holds user input
	private static Scanner stdin = new Scanner(System.in); //regular user input

	/**
	 * Runs the program with optional command line argument. The command line
	 * argument must be formatted as "fileName.txt"
	 * 
	 * @param args optional command line argument
	 */
	public static void main(String[] args) {
		
		//FILE READING SECTION
		//optional command line argument of an input file
		if(args.length == 1){
			String fileName = args[0];
			File file = new File(fileName);
			try {

				Scanner scnr = new Scanner(file);        //file input
				String fileInput = "x";
				while(scnr.hasNext()){
					System.out.println("Please enter a command (o, p, q, r):");
					fileInput = scnr.nextLine();
					
					
					switch(fileInput){
					case "o":	//output tree
						outputTree();
						break;
						
					case "p":	//play game
						playGame(scnr);
						break;
						
					case "q":	//quit game
						System.exit(0);
						
					case "r":	//reset game
						bTree = null;
						addQ(scnr);
						break;
					default: System.out.println("Invalid argument");
						
					}
					
				}
				scnr.close();
			} catch (FileNotFoundException e) {
				System.out.println("Cannot find the specified file");
			} 
		}
		if(args.length > 1){
			System.out.println("Program Usage: GuessingGame or Guessing" +
					"Game fileName.txt");
			System.exit(0);
		}
		
		//REGULAR INPUT SECTION
		String userInput = "x";                //x acts as a placeholder
		while(!userInput.equals("q")){
			System.out.println("Please enter a command (o, p, q, r):");
			userInput = stdin.nextLine();
			switch(userInput){

			//Outputs the Binary Tree
			case "o": 
				outputTree();
				break;

				//Plays the game
			case "p":
				playGame(stdin);
				break;
				
				//Quits the game
			case "q":
				break;
				
				//Resets the game
			case "r":
				bTree = null;
				addQ(stdin);
				break;
			default: System.out.println("Invalid argument");
			}
		}

		stdin.close();
	}

	/**
	 * This helper method adds a question into the tree. It prompts the user or
	 * a file for a question, true statement, and a false statement, and reads
	 * the answers into the bTree
	 * 
	 * @param scan tells the method whether to scan a file or System.in input
	 */
	private static void addQ(Scanner scan){
		try{
			System.out.println("Please enter a question.");
			bTree = new BinaryTree<String>(scan.nextLine());

			System.out.println("Please enter something that is " +
					"true for that question.");
			bTree.addLeftChild(scan.nextLine());

			System.out.println("Please enter something that is " +
					"false for that question.");
			bTree.addRightChild(scan.nextLine());

		} catch(IllegalBinaryTreeOpException e){
			//The children are not added
		}
	}
	
	/**
	 * The program will attempt to guess the user's answer unless the binary
	 * tree is empty, in which case it prompts the user for a new question with
	 * additional answers
	 * 
	 * @param scan tells the method whether to scan for user input or
	 * file input
	 */
	private static void playGame(Scanner scan){
		String userInput;
		if(bTree == null){
			addQ(scan);
		}
		bTree.start();
		while(!bTree.isLeaf()){
			
			//Prompt with question
			try {
				System.out.println(bTree.getCurrent());
			} catch (IllegalBinaryTreeOpException e) {
			
			}

			//Read in user input (y/n/Y/N)
			userInput = scan.nextLine().toLowerCase();

			//Navigate Left/True, Right/False
			try{
				if(userInput.equals("y")){
					bTree.goLeft();
				}
				//else statement assumes user inputs valid input
				else{
					bTree.goRight();
				}
			} catch(IllegalBinaryTreeOpException e){
				//The tree will not continue down either branches
			}
		}
		try {
			System.out.println("I guess: " + bTree.getCurrent() + 
					". Was I right?");
		} catch (IllegalBinaryTreeOpException e) {
			//The guess is not printed to the console
		}
		userInput = scan.nextLine().toLowerCase();
		if(userInput.equals("y")){
			System.out.println("I win!");
		}
		else{
			System.out.println("Darn. Ok, tell me a question that" +
					" is true for your answer," +
					" but false for my guess.");
			userInput = scan.nextLine();
			String newQuestion = userInput;
			
			System.out.println( "Thanks! And what were you " +
					"thinking of?");
			userInput = scan.nextLine();
			String newTrueAns = userInput;
			try{
				String newFalseAns = bTree.getCurrent();
				
				bTree.changeCurrent(newQuestion);
				bTree.addLeftChild(newTrueAns);
				bTree.addRightChild(newFalseAns);
				
			} catch(IllegalBinaryTreeOpException e){
				//none of the children are added and the Current node 
				//remains unchanged.
			}
		}
	}
	
	/**
	 * Outputs the tree to the console. If the tree is empty, outputs 
	 * "Empty Tree"
	 */
	private static void outputTree(){
		if(bTree == null){
			System.out.println("Empty Tree");
		}
		else{
			bTree.print();
		}
	}

}
